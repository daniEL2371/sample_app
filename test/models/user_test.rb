require "test_helper"

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "example user", email:"user@example.com", password: "foobar",password_confirmation: "foobar")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should not be valid" do
    @user.name = '  '
    assert_not @user.valid?
  end

  test "email should not be valid" do
    @user.email = '  '
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "#{"a" * 256}@gmail.com"
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = 'a'*51
    assert_not @user.valid?
  end

  test "should accept valid emails" do 
    valid_addresses = %w[mire@example.com hena@gmail.org daniel@example.org joss@wizkids.cn]
    valid_addresses.each do | valid_address |
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "should reject inValid emails" do 
    invalid_addresses = %w[mire@example,com henagmail.org daniel@example.org. joss+wizkids.cn]
    invalid_addresses.each do | invalid_address |
      @user.email = invalid_address
      p invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email should be unique" do
    dup_user = @user.dup
    @user.save
    assert_not dup_user.valid?
  end

  test "password should not br valid [nonblack]" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end
  
  test "password should not be valid [minmum-length]" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  test 'authenticated? should return false on nil remember_token digest' do
   assert_not @user.authenticated?(:remember, '')
  end

  test 'micropost should be destroyed when user gets destroyed' do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")

    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end

  test 'comment should be destroyed when user gets destroyed' do
    @user.save
    @user.comments.create!(content: "Lorem ipsum", micropost_id: microposts(:orange).id)

    assert_difference 'Comment.count', -1 do
      @user.destroy
    end
  end
 
end
