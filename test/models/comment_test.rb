require "test_helper"

class CommentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:michael)
    @micropost = microposts(:orange)
    # @comment = Comment.new(content: "Glad you ate orange", micropost_id: @micropost.id, user_id: @user.id)
    @comment = @micropost.comments.build(content: "Glad you ate orange", user_id: @user.id)
  end

  test "Comment should be valid" do
    p @comment.user.name
    p @comment.micropost.content
    p "======================="
    assert @comment.valid?
  end

  test "User id should be prensent" do
    @comment.user_id = nil
    assert_not @comment.valid?
  end

  test "Micropost id should be present" do
    @comment.micropost_id = nil
    assert_not @comment.valid?
  end

  test "Content should be present" do
    @comment.content = nil
    assert_not @comment.valid?
  end

  test "Content length must not be greater then 120 characters" do
    @comment.content = "a" * 121
    assert_not @comment.valid?
  end

  test "Recent comment should appear first" do
    p comments(:most_recent)
    assert_equal comments(:most_recent), Comment.first
  end
end
