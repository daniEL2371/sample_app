require "test_helper"

class MicropostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
      @user = users(:michael) 
      @micropost = @user.microposts.build(content: "Lorem ipsum") 
  end

  test "micropost should be valid" do
    assert @micropost.valid?
  end

  test "user should not be nil" do
    @micropost.user_id = nil
    assert_not @micropost.valid?
  end

  test "content should be present" do
    @micropost.content = " "
    assert_not @micropost.valid?
  end
  
  test "content should be at most 140 characters" do
    @micropost.content = "a" * 141
    assert_not @micropost.valid?
  end

  test "order should be most recent first" do
    assert_equal microposts(:most_recent), Micropost.first
  end

  test 'comment should be destroyed when micropost gets destroyed' do
    @micropost.save
    @micropost.comments.create!(content: "Lorem ipsum", user_id: users(:archer).id)
    
    assert_difference 'Comment.count', -1 do
      @micropost.destroy
    end
  end
    
  
end
