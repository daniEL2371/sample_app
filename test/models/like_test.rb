require "test_helper"

class LikeTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:michael)
    @micropost = microposts(:orange)
    # @comment = Comment.new(content: "Glad you ate orange", micropost_id: @micropost.id, user_id: @user.id)
    @like = @micropost.likes.build(user_id: @user.id)
  end

  test "Like should be valid" do
    assert @like.valid?
  end

  test "User id should be prensent" do
    @like.user_id = nil
    assert_not @like.valid?
  end

  test "Micropost id should be present" do
    @like.micropost = nil
    assert_not @like.valid?
  end

  test "Micropost id user id combination should be unique" do
    @micropost2 = microposts(:cat_video)
    @user2 = users(:archer)
    @like2 = @micropost.likes.create!(user_id: @user.id)
    @like3 = @micropost.likes.create!(user_id: @user2.id)
    
    assert @like3.valid?
    @like4 = @micropost2.likes.create!(user_id: @user.id)
    assert @like4.valid?
  end

 
end
