Rails.application.routes.draw do
  scope '(:locale)', locale: /#{I18n.available_locales.join("|")}/ do
    get 'password_resets/new'
    get 'password_resets/edit'
    get 'sessions/new'
    root 'static_pages#home'
    get '/home', to: 'static_pages#home'
    get '/users', to: 'users#index'
    get '/help', to: 'static_pages#help'
    get '/about', to: 'static_pages#about'
    get '/contact', to: 'static_pages#contact'
    get '/signup', to: 'users#new'
    get '/search', to: 'users#search'
    get '/login', to: 'sessions#new'
    post '/login', to: 'sessions#create'
    delete '/logout', to: 'sessions#destroy'

    resources :users

    resources :users do
      member do
        get :following, :followers
      end
    end
    
    resources :microposts
    resources :comments
    resources :likes 

    
    resources :relationships, only: [:create, :destroy]

    # your routes here...
  end
  resources :account_activations, only: [:edit]
  resources :password_resets, only: %i[new create edit update]
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
