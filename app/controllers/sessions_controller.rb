class SessionsController < ApplicationController
  def new
  end

  def create
      given_email = params[:session][:email]
      given_password = params[:session][:password]
      is_remember = params[:session][:remember_me]
      
      userFound = User.find_by(email: given_email.downcase)
      if userFound && userFound.authenticate(given_password) && userFound.activated
        log_in userFound
        is_remember == '1' ?  remember(userFound) : forget(userFound)
        redirect_to userFound
      else
        flash.now[:danger] = 'Invalid email/password combination' # Not quite right!

        render 'new'
      end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
