class UsersController < ApplicationController
  before_action :logged_in_user, only: %i[edit update show index destroy]
  before_action :correct_user, only: %i[edit update]
  before_action :admin_user, only: [:destroy]

  def index
    # @users = User.all
    @users = User.paginate(page: params[:page])
  end

  def new; end

  def show
    @like = Like.new
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
    @comment = Comment.new
    # @comments = @user.microposts.build.comments.build
  end

  def search
    @users = User.where(['name LIKE ?', '%' + params[:search] + '%']).paginate(page: params[:page])
    render 'index'
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
    p "user: me: #{@user}"
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      flash[:success] = 'Profile Updated!'
      redirect_to user_url(@user)
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = 'User deleted'
    redirect_to users_url
  end

  def create
    @user = User.new(user_params)
    # did not understand this part

    if @user.save
      @user.send_activation_email
      flash[:info] = 'Please check your email to activate your account.'
      redirect_to root_url
      # log_in @user

      # i can do it this way
      # redirect_to "#{users_path}/#{@user.id}"
      # flash[:success] = "Welcome to the Sample App!"
      # redirect_to user_url(@user)
    else
      render 'new'
    end
  end

  def following
    @title = 'Following'
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = 'Followers'
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  # def logged_in_user
  #   if (!logged_in?)
  #     flash[:danger] = 'Please log in.'
  #     redirect_to login_url
  #   end
  # end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless @user && @user == current_user
  end

  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end
