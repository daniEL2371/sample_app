class CommentsController < ApplicationController
    before_action :logged_in_user, only: [:create, :destroy]
    before_action :correct_micropost, only: [:create]
    before_action :correct_user, only: [:destroy]
  def create
    @comment = @micropost.comments.build(user_id: current_user.id, content: params[:comment][:content])
    @user = @micropost.user
    @microposts = @user.microposts.paginate(page: params[:page])
    if @comment.save
        flash[:success] = "Commented"
        redirect_to  request.referrer
    else
        flash[:danger] = "Input valid comment!"
        redirect_to request.referrer
    end
  end
  

  def destroy
    @comment.destroy
    flash[:success] = "Comment deleted"
    redirect_to request.referrer || root_url
  end
  
  private
    def correct_user
        @comment = current_user.comments.find_by(id: params[:id])
        redirect_to root_url if @comment.nil?
    end

    def correct_micropost
        @micropost = Micropost.find_by(id: params[:micropost_id])
        redirect_to root_url if @micropost.nil?
    end

    def comment_params
        params.require(:comment).permit(:content, :micropost_id)
    end
end
