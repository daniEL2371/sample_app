class PasswordResetsController < ApplicationController
  before_action :get_user, only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]
  before_action :check_reset_expire, only: [:edit, :update]
  

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = "Email sent with password reset instructions"
      redirect_to root_url
    else
      flash.now[:danger] = "Email address not found"
      render 'new'
    end
  end
  
  def new
  end

  def edit
  end

  def update
    if params[ :user][ :password].empty? # Case (3)
      @user.errors.add(:password, "can't be empty")
      render 'edit'
    elsif @user.update(user_params) # Case (4)
      log_in @user
      flash[:success] = "Password has been reset."
      redirect_to @user
    else
      render 'edit' # Case (2)
    end
      
  end

  private 
    def get_user
      @user = User.find_by(email: params[:email])
    end
    def valid_user
      unless (@user && @user.activated? && @user.authenticated?( :reset, params[:id]))
        redirect_to root_url
      end
    end

    def user_params
      params.require( :user).permit( :password, :password_confirmation)
    end

    def check_reset_expire
      if @user.password_reset_expired?
        flash[:danger] = 'Reset link has expired'
        render 'new'
      end
    end
      
end
