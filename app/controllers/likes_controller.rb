class LikesController < ApplicationController
  before_action :logged_in_user, only: %i[create destroy]
  before_action :correct_micropost, only: [:create]
  before_action :correct_user, only: [:destroy]

  before_action :correct_micropost_edit_version, only: [:edit]

  def create
    current_user.likedComment?(@micropost)
    @like = @micropost.likes.build(user_id: current_user.id)
    @user = @micropost.user
    @microposts = @user.microposts.paginate(page: params[:page])
    if @like.save
      redirect_to request.referrer
    else
      redirect_to request.referrer
    end
  end

  def destroy
    @like.destroy
    redirect_to request.referrer
  end

  def edit
    current_user.likedComment?(@micropost)
    @like = @micropost.likes.build(user_id: current_user.id)
    @user = @micropost.user
    @microposts = @user.microposts.paginate(page: params[:page])
    if @like.save
      redirect_to request.referrer
    else
      redirect_to request.referrer
    end

  end

  private

  def correct_user
    p '000000000000000000000000000000000000000000000000000000000000000000000000000000000000'
    p params[:id]
    p '---------------------------------------------------------------------------------------'
    @like = current_user.likes.find_by(id: params[:id])
    redirect_to root_url if @like.nil?
  end

  def correct_micropost
    @micropost = Micropost.find_by(id: params[:micropost_id])
    redirect_to root_url if @micropost.nil?
  end

  def comment_params
    params.require(:comment).permit(:content, :micropost_id)
  end

  def correct_micropost_edit_version
    @micropost = Micropost.find_by(id: params[:id])
    redirect_to root_url if @micropost.nil?
  end
end
