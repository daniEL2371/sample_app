class StaticPagesController < ApplicationController
  def home
    @micropost = current_user.microposts.build if logged_in?
    if logged_in?
      @micropost = current_user.microposts.build
      @comment = @micropost.comments.build
      @like = Like.new
      # @comment = @current_user.comments.build
      # @comments = @micropost.comments.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end    
  end

  def help
  end

  def about
  end

  def contact    
  end

  
  
end
