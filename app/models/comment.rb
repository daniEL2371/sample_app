class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :micropost
  validates :micropost_id, presence: true
  validates :user_id, presence: true
  validates :content, presence: true, length: {maximum:120}
  default_scope ->  {order(created_at: :desc)}
  
end
